# Talkweb_Niobe_APP 组件

-   [简介](#section11660541594)
-   [目录](#section1464106163818)
-   [涉及仓](#section1718733212010)

## 简介<a name="section11660541594"></a>

Niobe提供了快速上手的指导文档、以及预备知识教程，现有13个案例教程，每个例程都有非常详细的注释，代码风格统一，按照从基本例程到高级例程的方式编排，方便初学者由浅入深逐步学习。开发者拿到工程后经过简单的编译和下载即可看到实验现象，后续会持续更新案例以及多种模块组合展示。

## 快速上手（开发环境）

想要快速的体验一下拓维信息Niobe开发板，学习OpenHarmony的源码`获取`、`编译`、`烧录`过程，强烈建议您学习以下教程，这能让您在环境安装环节省下不少时间。

- [所需工具及环境](docs/所需工具及环境.md)

- [Docker环境安装](docs/安装docker.md)

- [代码获取](docs/代码获取.md)

- [使用HB工具进行代码编译](docs/代码编译.md)

- [使用HiBurn工具进行固件下载](docs/烧录指导.md)


## 预备知识教程系列

为了让你快速并系统的学习OpenHarmony系统的环境搭建，并想了解OpenHarmony整个系统从0到1的过程，建议您跟着我们一步一步的学习。

需要告诉您的是，OpenHarmony系统的环境搭建所涉及的领域及组件会比较多，您需要严格按照我们的教程执行每一步，否则可能会出现不能预知的错误。

- [OpenHarmony系统开发—线程](app/TW002_OS_thread/README.md)
- [OpenHarmony系统开发—定时器](app/TW003_OS_timer/README.md)
- [OpenHarmony系统开发—事件](app/TW004_OS_event/README.md)
- [OpenHarmony系统开发—互斥锁](app/TW005_OS_mutex/README.md)
- [OpenHarmony系统开发—信号量](app/TW006_OS_semp/README.md)
- [OpenHarmony系统开发—消息队列](app/TW007_OS_message/README.md)
- [OpenHarmony驱动子系统开发—GPIO](docs/OpenHarmony驱动子系统开发—GPIO.md)
- [OpenHarmony驱动子系统开发—GPIO中断](app/TW102_EXTI_key/README.md)
- [OpenHarmony驱动子系统开发—PWM](app/TW102_EXTI_key/README.md)
- [OpenHarmony驱动子系统开发—ADC](app/TW104_ADC_voltage/README.md)
- [OpenHarmony驱动子系统开发—IIC](app/TW105_I2C_sht30/README.md)
- [OpenHarmony驱动子系统开发—UART](app/TW106_UART/README.md)

## Niobe开发板案例介绍

拓维信息Niobe开发板系统提供多个案例程序，案例以TW1、TW2、TW3、TW4进行不同类别分级，方便开发板爱好者由浅入深逐步学习。您拿到系统源码后经过简单的编程和下载，即可看到实验现象。案例程序存放在系统源码`app/`目录下面，对应案例都文件夹命名方式按照`编号_类别_案例名称`方式进行，方便开发板爱好者寻找到对应源码 。

下面依次对TW1、TW2、TW3、TW4类进行简单介绍：

- TW001 - TW099：操作系统类
- TW100 - TW199：外设驱动类
- TW200 - TW299：模块应用类
- TW300 - TW399：物联通信类
- TW400 - TW499：综合应用类

案例列表如下所示：

| 编号  | 类别 | 案例名称  | 说明   | 模块 |
| ----- | ---- | ----------- | --------------------- |---------|
| TW001 | OS   | helloworld  | [第一个应用程序](app/TW001_OS_helloworld/README.md)        | 核心板|
| TW002 | OS   | os_thread   | [OS线程应用](app/TW002_OS_thread/README.md)           | 核心板|
| TW003 | OS   | os_timer    | [OS定时器应用](app/TW003_OS_timer/README.md)          | 核心板|
| TW004 | OS   | os_event    | [OS事件应用](app/TW004_OS_event/README.md)           | 核心板|
| TW005 | OS   | os_mutex    | [OS互斥锁应用](app/TW005_OS_mutex/README.md)       | 核心板|
| TW006 | OS   | os_semp     | [OS信号量应用](app/TW006_OS_semp/README.md)           | 核心板|
| TW007 | OS   | os_message  | [OS消息队列应用](app/TW007_OS_message/README.md)         | 核心板|
| TW101 | GPIO | gpio_led    | [GPIO点亮LED灯](app/TW101_GPIO_led/README.md)  | 核心板|
| TW102 | EXTI | exti_key    | [GPIO按键事件中断](app/TW102_EXTI_key/README.md)   | 核心板|
| TW103 | PWM  | pwm_led     | [PWM点亮LED呼吸灯](app/TW103_PWM_led/README.md)     | 核心板|
| TW104 | ADC  | adc_voltage | [ADC电压采集](app/TW104_ADC_voltage/README.md)      | 核心板 + OLED扩展板|
| TW105 | I2C  | i2c_sht30   | [I2C温湿度传感器采集](app/TW105_I2C_sht30/README.md)    | 核心板 + 马达扩展板|
| TW106 | UART | uart        | [UART串口自发自收](app/TW106_UART/README.md)       | 核心板 |

## 目录<a name="section1464106163818"></a>

```
applications/   # Niobe模块目录  

└── app         //示例代码
└── docs        //教程文档
└── figures     //存放相关图片
```

## 涉及仓<a name="section1718733212010"></a>

devboard_applications_sample_talkweb_niobe
