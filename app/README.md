# Niobe WiFi IoT开发板案例介绍

拓维信息Niobe开发板系统提供多个案例程序，案例以TW1、TW2、TW3、TW4进行不同类别分级，方便开发板爱好者由浅入深逐步学习。您拿到系统源码后经过简单的编程和下载，即可看到实验现象。案例程序存放在系统源码`applications/app/`目录下面，对应案例都文件夹命名方式按照`编号_类别_案例名称`方式进行，方便开发板爱好者寻找到对应源码 。

下面依次对TW1、TW2、TW3、TW4类进行简单介绍：

- TW001 - TW099：操作系统类
- TW100 - TW199：外设驱动类
- TW200 - TW299：模块应用类
- TW300 - TW399：物联通信类
- TW400 - TW499：综合应用类

案例列表如下所示：

| 编号  | 类别 | 案例名称  | 说明   | 模块 |
| ----- | ---- | ----------- | --------------------- |---------|
| TW001 | OS   | helloworld  | [第一个应用程序](applications/app/TW001_OS_helloworld/README.md)        | 核心板|
| TW002 | OS   | os_thread   | [OS线程应用](applications/app/TW002_OS_thread/README.md)           | 核心板|
| TW003 | OS   | os_timer    | [OS定时器应用](applications/app/TW003_OS_timer/README.md)          | 核心板|
| TW004 | OS   | os_event    | [OS事件应用](applications/app/TW004_OS_event/README.md)           | 核心板|
| TW005 | OS   | os_mutex    | [OS互斥锁应用](applications/app/TW005_OS_mutex/README.md)       | 核心板|
| TW006 | OS   | os_semp     | [OS信号量应用](applications/app/TW006_OS_semp/README.md)           | 核心板|
| TW007 | OS   | os_message  | [OS消息队列应用](applications/app/TW007_OS_message/README.md)         | 核心板|
| TW101 | GPIO | gpio_led    | [GPIO点亮LED灯](applications/app/TW101_GPIO_led/README.md)  | 核心板|
| TW102 | EXTI | exti_key    | [GPIO按键事件中断](applications/app/TW102_EXTI_key/README.md)   | 核心板|
| TW103 | PWM  | pwm_led     | [PWM点亮LED呼吸灯](applications/app/TW103_PWM_led/README.md)     | 核心板|
| TW104 | ADC  | adc_voltage | [ADC电压采集](applications/app/TW104_ADC_voltage/README.md)      | 核心板 + OLED扩展板|
| TW105 | I2C  | i2c_sht30   | [I2C温湿度传感器采集](applications/app/TW105_I2C_sht30/README.md)    | 核心板 + 马达扩展板|
| TW106 | UART | uart        | [UART串口自发自收](applications/app/TW106_UART/README.md)       | 核心板 |