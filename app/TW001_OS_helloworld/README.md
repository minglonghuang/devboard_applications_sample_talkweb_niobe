# 第一个应用程序

## Hello World

沿袭程序界的传统，第一个程序都是Hello World。在Niobe WiFi IoT开发板中，指的是通过开发板串口，在串口日志中输出Hello World字符串。

在Niobe WiFi IoT开发板系统代码中，Hello World程序位于applications目录下面，具体路径如下：

```tex
applications/app/TW001_OS_helloworld/hello_world.c
```
## 编译调试

#### 修改 BUILD.gn 文件

修改 `applications\app`路径下 BUILD.gn 文件，指定 `helloworld` 参与编译。

```C
 "TW001_OS_helloworld:helloworld",
#"TW002_OS_thread:os_thread_example",
#"TW003_OS_timer:os_timer_example",
#"TW004_OS_event:os_event_example",
#"TW005_OS_mutex:os_mutex_example",
#"TW006_OS_semp:os_semp_example",
#"TW007_OS_message:os_message_example",
#"TW101_GPIO_led:gpio_led_example",
#"TW102_EXTI_key:exti_key_example",
#"TW103_PWM_led:pwm_led_example",
#"TW104_ADC_voltage:adc_voltage_example",
#"TW105_I2C_sht30:i2c_sht30_example",
#"TW106_UART:uart_example",
#"TW301_APP_oled:app_oled_example",
#"TW302_APP_nfc:app_nfc_example"
```

#### 运行结果

示例代码编译烧录代码后，按下开发板的RESET按键，通过串口助手查看日志，
```c
[Talkweb] Hello world!
[Talkweb] Hello world!

```
