# GPIO点亮LED灯

本案例程序是演示GPIO接口的基本使用，案例操作结果是点亮开发板上的LED等，可以实现不断闪烁的效果。

本案例使用的LED灯根据开发板原理图可以得到，接入的是GPIO9号引脚。所以本次开发驱动的GPIO引脚是GPIO9。

LED灯在开发板的位置如下图红色框框标记所示：

![image-20210922154302998](figure/image-20210922154302998.png)

## GPIO函数使用

### IoTGpioInit()

![image-20210922154929304](figure/image-20210922154929304.png)

IoTGpioInit函数是一个初始化GPIO管脚的函数，函数返回初始化结果。此函数在管脚使用之前调用，避免重复调用，避在再中断中使用。

函数参数值传递需要使用到的引脚号，可选引脚参数有如下：

```C
typedef enum {
    IOT_GPIO_IO_GPIO_0 = 0,
    IOT_GPIO_IO_GPIO_1,
    IOT_GPIO_IO_GPIO_2,
    IOT_GPIO_IO_GPIO_3,
    IOT_GPIO_IO_GPIO_4,
    IOT_GPIO_IO_GPIO_5,
    IOT_GPIO_IO_GPIO_6,
    IOT_GPIO_IO_GPIO_7,
    IOT_GPIO_IO_GPIO_8,
    IOT_GPIO_IO_GPIO_9,
    IOT_GPIO_IO_GPIO_10,
    IOT_GPIO_IO_GPIO_11,
    IOT_GPIO_IO_GPIO_12,
    IOT_GPIO_IO_GPIO_13,
    IOT_GPIO_IO_GPIO_14,
    IOT_GPIO_IO_GPIO_MAX
} IOT_GPIO_IO_NAME;
```

### IoTGpioSetDir()

![image-20210922155739213](figure/image-20210922155739213.png)

IoTGpioSetDir函数用来设置GPIO引脚方向，函数传递两个参数，一个是需要设置的引脚号，一个是引脚方向。函数返回设置结果。

| 参数 | 描述                             |
| ---- | -------------------------------- |
| id   | 引脚号，参考初始化函数的引脚设置 |
| dir  | 引脚方向，参考IotGpioDir枚举值。 |

```C
/**
 * @brief Enumerates GPIO directions.
 */
typedef enum {
    /** Input */
    IOT_GPIO_DIR_IN = 0,
    /** Output */
    IOT_GPIO_DIR_OUT
} IotGpioDir;
```

### IoTGpioSetOutputVal()

![image-20210922160459647](figure/image-20210922160459647.png)

IoTGpioSetOutputVal函数用来设置引脚输出电平，函数传递两个参数，一个是需要设置的引脚号，一个是输出电平。函数返回设置结果。

| 参数 | 描述                                                   |
| ---- | ------------------------------------------------------ |
| id   | 引脚号，参考初始化函数的引脚设置                       |
| val  | 引脚输出电平，参考IotGpioValue枚举，即高电平和低电平。 |

```c
/**
 * @brief Enumerates GPIO level values.
 */
typedef enum {
    /** Low GPIO level */
    IOT_GPIO_VALUE0 = 0,
    /** High GPIO level */
    IOT_GPIO_VALUE1
} IotGpioValue;
```

## 案例程序解析

本案例通过不断循环给GPIO9引脚输出高电平和低电平，来实现LED灯的不断闪烁。LedLightTask函数指针是线程执行函数。本例的while循环需要在线程内循环，不可以直接在非线程内循环。

```c
static void *LedLightTask(const char *arg)
{
    (void)arg;

    printf("-----entry gpio demo -----\r\n");

    IoTGpioInit(IOT_GPIO_IO_GPIO_9);
    IotIoSetFunc(IOT_GPIO_IO_GPIO_9, HI_IO_FUNC_GPIO_9_GPIO);
    IoTGpioSetDir(IOT_GPIO_IO_GPIO_9, IOT_GPIO_DIR_OUT);

    while (1)
    {
        IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_9, IOT_GPIO_VALUE0);
        usleep(LED_INTERVAL_TIME_US);
        IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_9, IOT_GPIO_VALUE1);
        usleep(LED_INTERVAL_TIME_US);
    }

    return NULL;
}
```

## 编译调试

#### 修改 BUILD.gn 文件

修改 `applications\app`路径下 BUILD.gn 文件，指定 `gpio_led_example` 参与编译。

```C
# "TW001_OS_helloworld:helloworld",
#"TW002_OS_thread:os_thread_example",
#"TW003_OS_timer:os_timer_example",
#"TW004_OS_event:os_event_example",
#"TW005_OS_mutex:os_mutex_example",
#"TW006_OS_semp:os_semp_example",
#"TW007_OS_message:os_message_example",
"TW101_GPIO_led:gpio_led_example",
#"TW102_EXTI_key:exti_key_example",
#"TW103_PWM_led:pwm_led_example",
#"TW104_ADC_voltage:adc_voltage_example",
#"TW105_I2C_sht30:i2c_sht30_example",
#"TW106_UART:uart_example",
#"TW301_APP_oled:app_oled_example",
#"TW302_APP_nfc:app_nfc_example"
```

修改好BUILD.gn，代码编译烧录代码后，按下开发板的RESET按键。