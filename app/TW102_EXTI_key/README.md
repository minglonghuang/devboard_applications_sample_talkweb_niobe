# GPIO按键事件中断

本案例程序是演示GPIO中断接口的基本使用，案例操作结果是通过按键切换开发板上的LED的点亮和熄灭效果。

本案例使用的按键根据开发板原理图可以得到，接入的是GPIO5号引脚。所以本次开发驱动的GPIO引脚是GPIO5。

LED灯和按键在开发板的位置如下图红色框框标记所示：

![image-20210922170902858](figure/image-20210922170902858.png)

## GPIO函数使用

### IoTGpioRegisterIsrFunc()

![image-20210922171201833](figure/image-20210922171201833.png)

IoTGpioRegisterIsrFunc函数是一个设置GPIO引脚中断的函数，函数返回初始化结果。此函数在管脚使用之前调用，避免重复调用，避在再中断中使用。

**描述：**

启用GPIO引脚的中断功能。这个函数可以用来为GPIO pin设置中断类型、中断极性和中断回调。

**参数：**

| 名字        | 描述                               |
| ----------- | ---------------------------------- |
| id          | 表示GPIO引脚号.                    |
| intType     | 表示中断类型.                      |
| intPolarity | 表示中断触发方式.                  |
| func        | 表示中断回调函数.                  |
| arg         | 表示中断回调函数中使用的参数的指针 |

## 案例程序解析

本案例通过按键按压时触发的边沿中断，在中断回调函数中更改LED灯的输出电平，来达到按压一次按键，实现点灯和熄灯的效果。

由于开发板物理按键存在抖动的问题，会在按压一次按键时，多次触发中断回调，所以我们需要在中断回调函数中，进行软件防抖操作，过滤掉一些不必要的操作。

```c
static unsigned int lastTickCount = 0;
static unsigned int led_Level = 0;

//GPIO5中断回调函数
void GpioPressedIsrFunc(char *arg)
{
    (void)arg;

    //消除按键抖动
    unsigned int tickCount = osKernelGetTickCount();
    unsigned int count = tickCount - lastTickCount;
    lastTickCount = tickCount;

    if (count > 50)
    {
        led_Level ^= 1;
        IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_9, led_Level);
    }
}

static void *ClickKeyTask(const char *arg)
{
    (void)arg;
    unsigned int ret;

    printf("----- gpio isr demo -----\r\n");

    IoTGpioInit(IOT_GPIO_IO_GPIO_9);
    IotIoSetFunc(IOT_GPIO_IO_GPIO_9, HI_IO_FUNC_GPIO_9_GPIO);
    IoTGpioSetDir(IOT_GPIO_IO_GPIO_9, IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_9, IOT_GPIO_VALUE0);

    //初始化GPIO5引脚
    IoTGpioInit(IOT_GPIO_IO_GPIO_5);
    IotIoSetFunc(IOT_GPIO_IO_GPIO_5, HI_IO_FUNC_GPIO_5_GPIO);
    IotIoSetPull(IOT_GPIO_IO_GPIO_5, IOT_IO_PULL_UP);
    IoTGpioSetDir(IOT_GPIO_IO_GPIO_5, IOT_GPIO_DIR_IN);

    //设置GPIO5中断
    ret = IoTGpioRegisterIsrFunc(IOT_GPIO_IO_GPIO_5, IOT_INT_TYPE_EDGE,
                                 IOT_GPIO_EDGE_FALL_LEVEL_LOW, GpioPressedIsrFunc, NULL);
    if (ret != RET_OK)
    {
        printf("===== ERROR ======gpio -> hi_gpio_register_isr_function ret:%d\r\n", ret);
    }

    return NULL;
}
```

## 编译调试

#### 修改 BUILD.gn 文件

修改 `applications\app`路径下 BUILD.gn 文件，指定 `exti_key_example` 参与编译。

```C
# "TW001_OS_helloworld:helloworld",
#"TW002_OS_thread:os_thread_example",
#"TW003_OS_timer:os_timer_example",
#"TW004_OS_event:os_event_example",
#"TW005_OS_mutex:os_mutex_example",
#"TW006_OS_semp:os_semp_example",
#"TW007_OS_message:os_message_example",
#"TW101_GPIO_led:gpio_led_example",
"TW102_EXTI_key:exti_key_example",
#"TW103_PWM_led:pwm_led_example",
#"TW104_ADC_voltage:adc_voltage_example",
#"TW105_I2C_sht30:i2c_sht30_example",
#"TW106_UART:uart_example",
#"TW301_APP_oled:app_oled_example",
#"TW302_APP_nfc:app_nfc_example"
```

#### 运行结果

示例代码编译烧录代码后，按下开发板的RESET按键，开发板开始正常工作，此时LED会正常点亮，再按下按键LED会熄灭，再按下按键LED会重新点亮。