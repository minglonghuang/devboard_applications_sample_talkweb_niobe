# PWM点亮LED呼吸灯

本案例程序是演示PWM接口的基本使用，案例操作结果是点亮开发板上的LED灯，可以实现不断闪烁，类似LED灯在呼吸的效果。

本案例使用的LED灯根据开发板原理图可以得到，接入的是GPIO9号引脚。所以本次开发驱动的GPIO引脚是GPIO9。

LED灯在开发板的位置如下图红色框框标记所示：

![image-20210922154302998](figure/image-20210922154302998.png)

## PWM函数使用

### IoTPwmInit()

![image-20210922192026547](figure/image-20210922192026547.png)

IoTPwmInit函数是一个初始化PWM管脚的函数，函数返回初始化结果。此函数在管脚使用之前调用，避免重复调用，避在再中断中使用。

函数参数值传递需要使用到的引脚号，可选引脚参数有如下：

![image-20210922192210022](figure/image-20210922192210022.png)

本次案例使用的是接入了GPIO9号引脚的LED，从芯片手册可以得知，GPIO9使用的是PWM0，所以本次使用`HI_PWM_PORT_PWM0`作为参数。

### IoTPwmStart()

![image-20210922192610445](figure/image-20210922192610445.png)

IoTPwmStart函数根据输入参数输出PWM信号。可以实现控制引脚上的电压效果。

| 参数 | 描述      |
| ---- | --------- |
| port | PWM端口号 |
| duty | 占空比    |
| freq | 分频倍数  |

## 案例程序解析

本案例程序是在之前的[GPIO点灯案例](../TW101_GPIO_led/README.md)的基础上继续增强实现的。本次控制GPIO引脚输出方式使用了PWM信号来实现，通过更改PWM的空占比，输出不同的PWM波形，进而更改GPIO引脚上的电平电压。

如下案例程序函数，主要是在while循环中不断对duty空占比进行加减运算。

```c
static void *LedLightTask(const char *arg)
{
    (void)arg;

    printf("-----entry pwm demo -----\r\n");

    IoTGpioInit(IOT_GPIO_IO_GPIO_9);
    IotIoSetFunc(IOT_GPIO_IO_GPIO_9, HI_IO_FUNC_GPIO_9_PWM0_OUT);
    IoTGpioSetDir(IOT_GPIO_IO_GPIO_9, IOT_GPIO_DIR_OUT);

    IoTPwmInit(HI_PWM_PORT_PWM0);

    unsigned short duty = 0;
    unsigned int freq = 15000;
    static char plus_status = 0;

    while (1)
    {
        if (duty >= 99)
        {
            plus_status = 0;
        }
        else if (duty <= 0)
        {
            plus_status = 1;
        }

        if (plus_status)
        {
            duty++;
        }
        else
        {
            duty--;
        }

        usleep(LED_INTERVAL_TIME_US);
        IoTPwmStart(HI_PWM_PORT_PWM0, duty, freq);
    }

    return NULL;
}
```

## 编译调试

#### 修改 BUILD.gn 文件

修改 `applications\app`路径下 BUILD.gn 文件，指定 `pwm_led_example` 参与编译。

```C
# "TW001_OS_helloworld:helloworld",
#"TW002_OS_thread:os_thread_example",
#"TW003_OS_timer:os_timer_example",
#"TW004_OS_event:os_event_example",
#"TW005_OS_mutex:os_mutex_example",
#"TW006_OS_semp:os_semp_example",
#"TW007_OS_message:os_message_example",
#"TW101_GPIO_led:gpio_led_example",
#"TW102_EXTI_key:exti_key_example",
"TW103_PWM_led:pwm_led_example",
#"TW104_ADC_voltage:adc_voltage_example",
#"TW105_I2C_sht30:i2c_sht30_example",
#"TW106_UART:uart_example",
#"TW301_APP_oled:app_oled_example",
#"TW302_APP_nfc:app_nfc_example"
```

### 运行结果

示例代码编译烧录代码后，按下开发板的RESET按键，开发板开始正常工作，此时LED会进入呼吸状态，LED亮度由弱变强，再由强变弱，不断循环。

