# OpenHarmony驱动子系统开发—ADC电压采集

## 概述
    ADC，全称为Analog-to-Digital Converter，是一种模/数转换器，它可以将连续变化的模拟信号转换为离散的数字信号。

## 接口说明
    1. adc值采集函数:unsigned int IoTAdcRead(WifiIotAdcChannelIndex channel, unsigned short *data,      WifiIotAdcEquModelSel equ_model, WifiIotAdcCurBais cur_bais, unsigned short delay_cnt);
        参数说明: 
            channel:   adc转换通道号
            data:      保存adc采集值的数据指针
            equ_model: 用于做平均值算法的采集次数
            cur_bais:  功率模式选择
            delay_cnt: 从adc初始化到转换开始的延时时间计数
            return:    adc采集结果返回，具体请参考hi_errno.h

    2. adc值转换函数:float IOTAdcConvertToVoltage(unsigned short data);
        参数说明: 
            data:   采集的adc值
            return: float型的对应电压值

## 例程原理简介
    本例程通过切换按下SW6/SW7按键，改变LCD_GPIO_05引脚处的电压分压值，从而改变adc的采集值。