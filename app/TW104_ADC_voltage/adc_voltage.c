/*
 * Copyright (c) 2021 Talkweb Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <stdio.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "hi_errno.h"
#include "iot_adc.h"
#include "iot_gpio.h"

#define ADC_TASK_STACK_SIZE 4096
#define ADC_TASK_PRIO 25

static void adcTask(void)
{
    float voltage;
    unsigned short val;
    
    IoTGpioInit(IOT_GPIO_IO_GPIO_5);
    IotIoSetPull(IOT_GPIO_IO_GPIO_5, IOT_IO_PULL_NONE);

    for(;;)
    {
        //获取ADC采集值
        unsigned int ret = IoTAdcRead(WIFI_IOT_ADC_CHANNEL_2,&val,WIFI_IOT_ADC_EQU_MODEL_8,WIFI_IOT_ADC_CUR_BAIS_DEFAULT,256);
        if(ret != HI_ERR_SUCCESS)
        {
            printf("Failed to read adc! Err code = %u\n", ret);
            continue;
        }

       //将ADC采集值转换成电压值
        voltage = IOTAdcConvertToVoltage(val);
        printf("voltage:%.3fV \r\n", voltage);

        usleep(1000000);
    }
}

static void adcVoltageEntry(void)
{
    osThreadAttr_t attr;
    attr.name = "adcTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = ADC_TASK_STACK_SIZE;
    attr.priority = ADC_TASK_PRIO;

    if (osThreadNew((osThreadFunc_t)adcTask, NULL, &attr) == NULL) {
        printf("[adcExample] Falied to create adcTask!\n");
    }
	return;
}

APP_FEATURE_INIT(adcVoltageEntry);
