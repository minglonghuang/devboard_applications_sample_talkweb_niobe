# OpenHarmony驱动子系统开发—I2C总线

## 概述
    I2C(Inter Integrated Circuit)总线是由Philips公司开发的一种简单、双向二线制同步串行总线。
    I2C以主从方式工作，通常有一个主设备和一个或者多个从设备，主从设备通过SDA(SerialData)串行数据线以及SCL(SerialClock)串行时钟线两根线相连，如图1 所示。
    I2C数据的传输必须以一个起始信号作为开始条件，以一个结束信号作为传输的停止条件。数据传输以字节为单位，高位在前，逐个bit进行传输。
    I2C总线上的每一个设备都可以作为主设备或者从设备，而且每一个设备都会对应一个唯一的地址，当主设备需要和某一个从设备通信时，通过广播的方式，将从设备地址写到总线上，如果某个从设备符合此地址，将会发出应答信号，建立传输。
    I2C接口定义了完成I2C传输的通用方法集合，包括：
    I2C控制器管理: 打开或关闭I2C控制器
    I2C消息传输：通过消息传输结构体数组进行自定义传输

## 接口说明
   1. I2C初始化函数:unsigned int IoTI2cInit(unsigned int id, unsigned int baudrate)
        参数说明: 
            id:       对应的I2C号
            baudrate: I2C通信速率
            return:   IOT_SUCCESS表示初始化成功

    2. I2C去初始化函数:unsigned int IoTI2cDeinit(unsigned int id)
        参数说明: 
            id:      对应的I2C号
            return:   IOT_SUCCESS表示去除初始化成功

    3. I2C写数据函数:unsigned int IoTI2cWrite(unsigned int id, unsigned short deviceAddr, const   unsigned char *data, unsigned int dataLen)
        参数说明: 
            id:         对应的I2C号
            deviceAddr: I2C设备地址
            data:       写入的数据缓冲区地址
            dataLen:    写入的数据字节数
            return:     IOT_SUCCESS表示写入成功

    4. I2C读数据函数:unsigned int IoTI2cRead(unsigned int id, unsigned short deviceAddr, unsigned char *data, unsigned int dataLen)
        参数说明: 
            id:         对应的I2C号
            deviceAddr: I2C设备地址
            data:       读取数据缓冲区地址
            dataLen:    读取的字节数
            return:     IOT_SUCCESS表示读取成功

    5. I2C速率设置函数:unsigned int IoTI2cSetBaudrate(unsigned int id, unsigned int baudrate)
        参数说明: 
            id:       对应的I2C号  
            baudrate: 设置的I2C速率值     

## 例程原理简介
    本例程通过I2C接口函数，与SHT30温湿度传感器设置进行实时通信，获取SHT30的温/湿度值，并通过串口实时打印出来。
