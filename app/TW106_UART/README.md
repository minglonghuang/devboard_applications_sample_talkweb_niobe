# OpenHarmony驱动子系统开发—ADC电压采集

## 概述
    1.UART是通用异步收发传输器（Universal Asynchronous Receiver/Transmitter）的缩写，是通用串行数据总线，用于异步通信。该总线双向通信，可以实现全双工传输。

    2.UART与其他模块一般用2线或4线相连:
    当使用2线相连时，它们分别是：
        TX：发送数据端，和对端的RX相连；
        RX：接收数据端，和对端的TX相连；

    当使用4线相连时，它们分别是：
        TX：发送数据端，和对端的RX相连；
        RX：接收数据端，和对端的TX相连；    
        RTS：发送请求信号，用于指示本设备是否准备好，可接受数据，和对端CTS相连；
        CTS：允许发送信号，用于判断是否可以向对端发送数据，和对端RTS相连；

    本例程我们使用2线相连的方式。

    3.UART通信之前，收发双方需要约定好一些参数：波特率、数据格式（起始位、数据位、校验位、停止位）等。通信过程中，UART通过TX发送给对端数据，通过RX接收对端发送的数据。

## 接口说明
    1. uart初始化函数:unsigned int IoTUartInit(unsigned int id, const IotUartAttribute *param)
        参数说明: 
            id:     对应的uart号，Hi3861芯片有三组uart，分别为0、1、2
            param:  uart初始化的基本参数，具体定义可查看IotUartAttribute结构体相关
            return: IOT_SUCCESS表示初始化成功

    2. uart读数据函数:int IoTUartRead(unsigned int id, unsigned char *data, unsigned int dataLen)
        参数说明: 
            id:      对应的uart号，Hi3861芯片有三组uart，分别为0、1、2
            data:    uart读取数据后的缓存地址
            dataLen：uart本次期望读取的数据长度
            return:  本次实际读取的数据字节数

    3. uart写数据函数:int IoTUartWrite(unsigned int id, const unsigned char *data, unsigned int dataLen)
        参数说明: 
            id:      对应的uart号，Hi3861芯片有三组uart，分别为0、1、2
            data:    uart发送数据的缓存地址
            dataLen：uart本次期望发送的数据长度
            return:  本次实际写入的数据字节数

    4. uart去初始化函数:int IoTUartWriteunsigned int IoTUartDeinit(unsigned int id)
        参数说明: 
            id:      去除初始化的uart号，Hi3861芯片有三组uart，分别为0、1、2

    5. uart流控设置函数:unsigned int IoTUartSetFlowCtrl(unsigned int id, IotFlowCtrl flowCtrl)
        参数说明: 
            id:       被设置的uart号，Hi3861芯片有三组uart，分别为0、1、2   
            flowCtrl: 具体流控设置选项           

## 例程原理简介
    本例程通过将uart的TX连接RX，实现uart的自发自收功能。