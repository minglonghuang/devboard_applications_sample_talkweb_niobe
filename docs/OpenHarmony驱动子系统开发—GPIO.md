# OpenHarmony驱动子系统开发—GPIO

## 概述

GPIO（General-purpose input/output）即通用型输入输出。通常，GPIO控制器通过分组的方式管理所有GPIO管脚，每组GPIO有一个或多个寄存器与之关联，通过读写寄存器完成对GPIO管脚的操作。

GPIO接口定义了操作GPIO管脚的标准方法集合，包括：

- 设置管脚方向： 方向可以是输入或者输出(暂不支持高阻态)
- 读写管脚电平值： 电平值可以是低电平或高电平
- 设置管脚中断服务函数：设置一个管脚的中断响应函数，以及中断触发方式
- 使能和禁止管脚中断：禁止或使能管脚中断

> GPIO接口定义在`base/iot_hardware/peripheral/interfaces/kits/iot_gpio.h`文件中

## 接口说明

| 功能分类              | 接口名                   | 描述                       |
| --------------------- | ------------------------ | -------------------------- |
| GPIO初始化            | IoTGpioInit              | 初始化管脚                 |
| GPIO初始化            | IoTGpioDeinit            | 取消管脚初始化             |
| 配置GPIO方向          | IoTGpioSetDir            | 设置管脚方向               |
| 配置GPIO方向          | IoTGpioGetDir            | 读取管脚方向               |
| GPIO读写              | IoTGpioSetOutputVal      | 设置管脚输出电平           |
| GPIO读写              | IoTGpioGetOutputVal      | 读取管脚输出电平           |
| GPIO读写              | IoTGpioGetInputVal       | 读取管脚输入电平           |
| 配置GPIO中断          | IoTGpioRegisterIsrFunc   | 设置管脚对应的中断服务函数 |
| 配置GPIO中断          | IoTGpioUnregisterIsrFunc | 取消管脚对应的中断服务     |
| 配置GPIO中断          | IoTGpioSetIsrMask        | 使能管脚中断功能           |
| 配置GPIO中断          | IoTGpioSetIsrMode        | 设置管脚的中断触发模式。   |
| GPIO使能配置          | IotIoGetFunc             | 获取管脚使能               |
| GPIO使能配置          | IotIoSetFunc             | 设置管脚使能               |
| 配置GPIO电阻上拉/下拉 | IotIoGetPull             | 获取管脚上拉/下拉          |
| 配置GPIO电阻上拉/下拉 | IotIoSetPull             | 设置管脚上拉/下拉          |

## 使用指导

#### 使用流程

GPIO标准API通过GPIO管脚号来操作指定管脚，使用GPIO的一般流程如下所示。

![点击放大](https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtyPub/011/111/111/0000000000011111111.20210916172618.06240802371375905258937880979642:50520915100716:2800:1E9853E26D0257053407BEA342DDB75F990BC22787B5E65F3962826A7A9FA0BA.png?needInitFileName=true?needInitFileName=true)

#### 确定GPIO管脚号

GPIO管脚号需要结合开发板的电路图来确定，具体开发板电路原理图的资料需要从开发板厂商获取。
