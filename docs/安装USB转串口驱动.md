# 安装USB转串口驱动

相关步骤在Windows工作台操作。

1. 点击链接[下载CH341SER USB转串口](http://www.hihope.org/download/download.aspx?mtt=8)驱动程序。

2. 点击安装包，安装驱动程序。

3. 驱动安装完成后，重新插拔USB接口，串口信息显示如下图所示。

<img src="figure/设备管理器.png" alt="设备管理器" style="zoom:150%;" />





